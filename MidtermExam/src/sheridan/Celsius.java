/*
 * @author Qingqing Wu, 991500423
 */


package sheridan;

public class Celsius {

	public static int fromFahrenheit(int argument) throws NumberFormatException {

		if (argument != 0 && argument < 1000 && argument > -460) {

			if ((argument - 32) * 5 % 9 == 0) {
				int celsius = (argument - 32) * 5 / 9;
				return celsius;
			}
			if ((argument - 32) * 5 % 9 != 0) {
				int celsius = (int) Math.ceil((argument - 32) * 5 / 9);
				return celsius;
			}

		}
		if (argument >= 1000 || argument <= -460) {
			throw new NumberFormatException();
		}
		
		return 0;
	}

}
