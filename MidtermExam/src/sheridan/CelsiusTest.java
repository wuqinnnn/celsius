/*
 * @author Qingqing Wu, 991500423
 */

package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void testFromFahrenheitRegular() {
		int celsius = Celsius.fromFahrenheit(55);
		assertTrue("Invalid Fahrenheit", celsius == 12);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testFromFahrenheitException() {
		int celsius = Celsius.fromFahrenheit(1000);
		fail("Invalid Fahrenheit");
	}
	
	@Test
	public void testFromFahrenheitBoundaryIn() {
		int celsius = Celsius.fromFahrenheit(55);
		assertTrue("Invalid Fahrenheit", celsius == 12);
	}
	
	@Test
	public void testFromFahrenheitBoundaryOut() {
		int celsius = Celsius.fromFahrenheit(55);
		assertFalse("Invalid Fahrenheit", celsius == 13);
	}

}
